CURRENT_SERIES_FILE=$(dirname -- $0)/current-series.simon
CURRENT_PLAYER_STEP_FILE=$(dirname -- $0)/current-player-step.simon

currentValues=$(cat $CURRENT_SERIES_FILE | tr '\n' ' ')
currentSeries=( $currentValues ) # convert to array

playerStep=$(cat $CURRENT_PLAYER_STEP_FILE)
playerStep=${playerStep:= 0} #value or 0 if empty

playerChoice=$1
requiredButton=${currentSeries[$playerStep]}

btns=(HI BYE)
newBtn=${btns[0]}
currentSeries=()
currentSeries+=($newBtn)

echo "Player Choice: $playerChoice"
echo "Required Button: $requiredButton"
echo "Current Level: ${#currentSeries[@]}"

if [[ $playerChoice == $requiredButton ]]; then
	echo "correct"
	((playerStep++))
	echo $playerStep > $CURRENT_PLAYER_STEP_FILE
else 
	echo "incorrect"
fi

osascript <<APPLESCRIPT
	tell application "BetterTouchTool" 
	update_touch_bar_widget "BC40B9B5-13CF-4C1F-A4FF-C433D1FFB4E0" text "${#currentSeries[@]}"
	end tell
APPLESCRIPT