# TODO
# - Could I have different beeps? Perhaps a toggle button for it?

DISPLAY_TIMER=0.35
BUTTON_NAMES=( GREEN RED YELLOW BLUE )

CURRENT_SERIES_FILE=$(dirname -- $0)/current-series.simon
CURRENT_PLAYER_STEP_FILE=$(dirname -- $0)/current-player-step.simon

GREEN="30D3310E-27FB-4A48-9210-FAA429F6F09E"
RED="9DFA2DED-70A3-43BB-895B-7EE678E35233"
YELLOW="C842BEAC-F35D-4FD3-BBAA-42BF3D8805EB"
BLUE="2883A42C-DAA5-4A81-BBC7-419B4D9B5680"

FEEDBACK="BC40B9B5-13CF-4C1F-A4FF-C433D1FFB4E0"

currentSeries=""

main() {

	clearAll

	if [ $1 ]; then
		
		slurpSeriesFile

		if [ "$1" == "RETRY" ]; then
			resetPlayerStep
			showCurrentLevel
			displayCurrentSeries
		else 
			playerTap $1
		fi
		
	else 
		# new game button
		>$CURRENT_SERIES_FILE #properly empty file without leaving an empty line

		slurpSeriesFile
		advanceRound
	fi
}

slurpSeriesFile() {
	currentValues=$(cat $CURRENT_SERIES_FILE | tr '\n' ' ')
	currentSeries=( $currentValues ) # convert to array
}

playerTap() {
	# toggleButton $1 
	# when this gets called, the delay is hitting when the if below is running
	# - so I guess shell is getting confused which code it's running?
	
	playerStep=$(cat $CURRENT_PLAYER_STEP_FILE)
	playerStep=${playerStep:= 0} #value or 0 if empty

	playerChoice=$1
	requiredButton=${currentSeries[$playerStep]}

	if [[ $playerChoice == $requiredButton ]]; then

		((playerStep++))
		echo $playerStep > $CURRENT_PLAYER_STEP_FILE

		if [[ $playerStep == "${#currentSeries[@]}" ]]; then
			advanceRound
		fi	

	else
		setButton FEEDBACK "NO"	
	fi
}

resetPlayerStep() {
	echo 0 > $CURRENT_PLAYER_STEP_FILE
}

showCurrentLevel() {
	setButton FEEDBACK ${#currentSeries[@]}
}

advanceRound() {
	
	resetPlayerStep	

	randIdx=$(( RANDOM % 4 ))
	newButton=${BUTTON_NAMES[$randIdx]}
	echo $newButton >> $CURRENT_SERIES_FILE 
	currentSeries+=($newButton)

	showCurrentLevel
	displayCurrentSeries
}

displayCurrentSeries() {
	for button in ${currentSeries[@]}
	do
		toggleButton $button
	done
}

showAll() { # mostly just for testing
	setButton GREEN COLOR
	setButton RED COLOR
	setButton YELLOW COLOR
	setButton BLUE COLOR
}

toggleButton() {
	color=$1

	setButton $color COLOR
	sleep $DISPLAY_TIMER
	setButton $color
}

setButton() {
	buttonId=${!1} # resolve incoming string to matching variable name above

	if [[ $2 == "COLOR" ]]; then
		text="$1"
	elif [ $2 ]; then
		text="$2"
	else
		text=" " # have to show a space to clear the button display
	fi

	# say $text # this works great but it slows everything down

	osascript <<APPLESCRIPT
	tell application "BetterTouchTool" 
	update_touch_bar_widget "$buttonId" text "$text"
	end tell
APPLESCRIPT
}

clearAll() {
	osascript <<APPLESCRIPT
	tell application "BetterTouchTool" 
	update_touch_bar_widget "$GREEN" text " "
	update_touch_bar_widget "$RED" text " "
	update_touch_bar_widget "$YELLOW" text " "
	update_touch_bar_widget "$BLUE" text " "
	end tell
APPLESCRIPT
}

main "$@"; exit # this lets me call functions before declaring them