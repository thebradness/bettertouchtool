
url="https://api.wordnik.com/v4/words.json/wordOfTheDay"
apiKeyFile=$(dirname -- $0)/word-of-the-day.apikey
apiKey=$(cat $apiKeyFile)

result=`curl --silent "$url?api_key=$apiKey"`

# echo $result

if [[ $result ]]; then
	
	theWord=$( 
			echo $result |\
			/opt/homebrew/bin/jq -r '.word' |\
			tr '[:upper:]' '[:lower:]' # "TRanslate upper to lower"
		)

	# $0 is the full path to the script, 
	# dirname takes it as an argument and returns the parent dir path
	echo $theWord > $(dirname -- $0)/word-of-the-day.stored 
	
	echo "WOTD: $theWord"

else

	echo "(no web)"

fi