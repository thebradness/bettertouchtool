# Basic Setup
1. Get a [MacBook with a Touchbar](https://www.apple.com/shop/buy-mac/macbook-pro/13-inch).
1. Clone this repo to ~/Documents/projects/bettertouchtool.
1. Download [BetterTouchTool](https://folivora.ai/)
	* Click **Preset** in the upper-right corner and import the **Touchbar.bttpreset** file from this repo.
1. Get [homebrew](https://brew.sh/) and install these brews...
	* `brew install blueutil` (for the bluetooth toggle button)
	* `brew install jq` (for easy parsing of the word of the day widget)
	* `brew install osx-cpu-temp` (for the cpu temp widget)
1. **Simon** is at the end of the scrolling / main bar.
1. Word of the Day widget
	* Get a (free) API Key from [API-Ninjas.com](https://api-ninjas.com/)
	* Stick it in **word-of-the-day/word-of-the-day.apikey**.
	* Tap for a new word, long press to look up that word.

# ...IGNORE THE REST OF THIS...

# System Settings
1. System Settings > Keyboard > Touchbar settings... (button)
	* Touch bar shows > app controls (better touch tool won't show at all without this)
	* Show control strip > false (so I can hide the whole thing easily)
	* Press and hold fn key to > show spaces

# Pandora Bar Setup
1. Download/install pianobar (pandora command line) and the temperature listener
	* `brew install pianobar`
1. Give the event receiver script execute rights
	* `chmod +x ~/Documents/projects/bettertouchtool/pianobar-event-receiver.sh`
1. Create named pipe file
	* `touch ~/.config/pianobar/ctl`
1. Setup pianobar config file
	* `vi ~/.config/pianobar/config`	
	* add pandora user/pass
	* add event command file line: `event_command = ~/Documents/projects/bettertouchtool/pianobar-event-receiver.sh`
	* add custom like icon: `love_icon = 👍`
1. Download/install watchman
	* `brew install watchman`
1. Create the nowplaying trigger
	* `cd ~/Documents/projects/bettertouchtool`
	* `watchman watch .`
	* `watchman -- trigger . refresh-nowplaying-widget '*nowplaying' -- osascript refresh-nowplaying-widget.scpt`