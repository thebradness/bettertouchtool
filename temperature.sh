
# <100 blue
# >100 green
# >180 yellow
# >200 red

white="255,255,255"
black="0,0,0"
blue="53,124,163"
green="87,135,38"
yellow="255,224,97"
red="206,35,43"



tempDisplay=$(/opt/homebrew/bin/osx-cpu-temp -F)

#get just the number before the decimal
tempNum=$(echo $tempDisplay | cut -d'.' -f1) 

# tempNum="100" #for testing


if (($tempNum < 100)); then
	fg=$blue
	bg=$white
elif (($tempNum < 150)); then
	fg=$white
	bg=$green
elif (($tempNum < 200)); then
	fg=$black
	bg=$yellow
else
	fg=$yellow
	bg=$red
fi

# echo $tempNum # uncomment to play with settings in the BTT config

echo "{
	\"text\":\"$tempDisplay\",
	\"font_color\": \"$fg,255\", 
	\"background_color\": \"$bg,255\",                                                 
}"