   
isRunning=$(pgrep pianobar) #check if pianobar process is running

if [ ! "$isRunning" ]; then
	#stuff in user path is not available from BTT so we can't just say "pianobar"
	echo "...STARTING..." > ~/Documents/projects/bettertouchtool/pianobar-nowplaying
	$(/usr/local/bin/pianobar)
else
	echo "$1" > ~/.config/pianobar/ctl
fi