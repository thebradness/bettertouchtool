#!/bin/sh
# this receives events from pianobar

#pianobar sends in a bunch of info about the song (and a list of your stations) through std in.
#so first stick it in a variable (because it'll be consumed the first time it's read)
#and then use the filter function to grab what I want out of it

BTT_PATH=~/'Documents/projects/bettertouchtool' # tilde needs to be expanded outside of quotes

info=$(cat /dev/stdin)

filter() { #echo is the equiv of return below
	echo $(echo "$info" |\
	grep "$1" |\
	cut -d '=' -f 2)
}


# echo "EVENT COMMAND -- $1" # for testing

case "$1" in #event 
	
	#on songlove, song info will be sent back in with rating=1, nowplaying file will update
	# - and watchman will refresh the touchbar widget
	#stationdeletefeedback is just the undoing of songlove

	userlogin)
		echo "...LOGGING IN..." > $BTT_PATH/pianobar-nowplaying
	;;

	usergetstations)
		echo "...RETRIEVING STATIONS..." > $BTT_PATH/pianobar-nowplaying
	;;

	stationfetchplaylist)
		echo "...RETRIEVING SONGS..." > $BTT_PATH/pianobar-nowplaying
	;;

	songstart | songlove) 
		echo $(filter stationName) > $BTT_PATH/pianobar-nowplaying-station

		#adding a thumb to the end will show it on the touchbar but 
		#it'll also be used in a regex in BTT to color it blue so it's obvious it's been liked
		thumb=""; if [ "$(filter rating)" = "1" ]; then thumb=" 👍"; fi

		echo \"$(filter title)\" by \"$(filter artist)\" on \"$(filter album)\" "("$(filter stationName)")" $thumb > $BTT_PATH/pianobar-nowplaying
	;;

	stationfetchplaylist)
		> $BTT_PATH/pianobar-station-list #emtpy the file
		stationCount=$(expr $(filter stationCount) - 1) #station count - 1

		for i in $( seq 0 $stationCount )
		do
			echo $(filter station$i=) >> $BTT_PATH/pianobar-station-list
		done
	;;
esac

echo "\n\nEVENT: $1 \n$info" > $BTT_PATH/pianobar-current-event # send everything to a file just for debugging


