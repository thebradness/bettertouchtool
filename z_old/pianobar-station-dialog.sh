
# this is easier than reading a file in an applescript because it just wouldn't happen

BTT_PATH=~/'Documents/projects/bettertouchtool' # this could just be $(dirname -- "$0")

currentStation=$(cat $BTT_PATH/pianobar-nowplaying-station)
stations=$(cat $BTT_PATH/pianobar-station-list | awk '{print "\""$0"\""}' | paste -s -d "," -)

choice=$(osascript <<APPLESCRIPT 
tell application "System Events"
activate
set chosenStation to choose from list {$stations} with prompt "Choose a new station:" with title "Change Station" default items {"$currentStation"} 
end tell
APPLESCRIPT
)

# result will be false if cancel is pressed
# we can't send that or the station menu will hang (in the pianobar process) and not play the next song
if [ "$choice" != "false" ]; then
	# echo "i" > ~/.config/pianobar/ctl
	echo "s$choice" > ~/.config/pianobar/ctl
fi