result=`curl -s "http://www.aramarkcafe.com/layouts/classic_new/locationhome.aspx?locationid=3297" |\

#look for an optional preceeding "zone_" or trailing "zone" to avoid words like grilled in the menu items
grep -i "\(zone_\)\?$1\(zone\)\?\." |\

#remove the anchor tag that's around SOME mune items
sed 's/\<a[^>]*\>//' | sed 's/\<\/a\>//' |\

#remove the rest of the surrounding HTML
sed 's/.*dailymenu"\>//' | sed 's/\<.*\>//' |\

#piping to xargs trims it, otherwise the ternary below will evaluate it as true/not empty
xargs`

#no menu shown on weekends - show "-" (shell ternary)
echo $([ "$result" ] && echo "$result" || echo "-")
